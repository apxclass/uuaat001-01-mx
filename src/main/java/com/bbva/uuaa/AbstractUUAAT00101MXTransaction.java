package com.bbva.uuaa;

import com.bbva.elara.transaction.AbstractTransaction;
import com.bbva.uuaa.dto.banco.CuentaDTO;
import com.bbva.uuaa.dto.banco.PaginationOutDTO;
import com.bbva.uuaa.dto.banco.paginationInDTO;
import java.util.List;

/**
 * In this class, the input and output data is defined automatically through the setters and getters.
 */
public abstract class AbstractUUAAT00101MXTransaction extends AbstractTransaction {

	public AbstractUUAAT00101MXTransaction(){
	}


	/**
	 * Return value for input parameter numCuenta
	 */
	protected Double getNumcuenta(){
		return (Double)this.getParameter("numCuenta");
	}

	/**
	 * Return value for input parameter coddivisa
	 */
	protected String getCoddivisa(){
		return (String)this.getParameter("coddivisa");
	}

	/**
	 * Return value for input parameter nuCuenta
	 */
	protected Long getNucuenta(){
		return (Long)this.getParameter("nuCuenta");
	}

	/**
	 * Return value for input parameter cdtipocuenta
	 */
	protected String getCdtipocuenta(){
		return (String)this.getParameter("cdtipocuenta");
	}

	/**
	 * Return value for input parameter importe
	 */
	protected Double getImporte(){
		return (Double)this.getParameter("importe");
	}

	/**
	 * Return value for input parameter paginationIn
	 */
	protected paginationInDTO getPaginationin(){
		return (paginationInDTO)this.getParameter("paginationIn");
	}

	/**
	 * Set value for String output parameter operacion
	 */
	protected void setOperacion(final String field){
		this.addParameter("operacion", field);
	}

	/**
	 * Set value for List<CuentaDTO> output parameter listCuentas
	 */
	protected void setListcuentas(final List<CuentaDTO> field){
		this.addParameter("listCuentas", field);
	}

	/**
	 * Set value for PaginationOutDTO output parameter paginationOut
	 */
	protected void setPaginationout(final PaginationOutDTO field){
		this.addParameter("paginationOut", field);
	}
}
