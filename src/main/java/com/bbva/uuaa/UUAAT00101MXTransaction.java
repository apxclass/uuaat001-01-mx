package com.bbva.uuaa;


import com.bbva.elara.domain.transaction.Advice;
import com.bbva.elara.domain.transaction.Severity;
import com.bbva.uuaa.dto.banco.CuentaDTO;
import com.bbva.uuaa.dto.banco.PaginationInDTO;
import com.bbva.uuaa.dto.banco.PaginationOutDTO;
import com.bbva.uuaa.lib.r001.UUAAR001;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ES
 *
 */
public class UUAAT00101MXTransaction extends AbstractUUAAT00101MXTransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(UUAAT00101MXTransaction.class);

	/**
	 * The execute method...
	 */
	@Override
	public void execute() {
		UUAAR001 uuaaR001 = this.getServiceLibrary(UUAAR001.class);
		// TODO - Implementation of business logic
		//lestura de parametros de entrada
		//validacion
		//respuesta
		
		PaginationInDTO paginationIn = new PaginationInDTO();
		
		Double cuenta =this.getNumcuenta(); //dato que jala de los datos de entrada 
		
	    Double paginationIn= this.getPaginationin();
		
		
		LOGGER.info(" numero de cuenta {}", cuenta);
		//LOGGER.info("tipo de divisa", divisa);
		//LOGGER.info("importe", importe);
		//LOGGER.info("importe", tipoCuenta);
		
		LOGGER.info("INICIA LA TRANSACCION 1");
		
		CuentaDTO accountDTO = new CuentaDTO();
		
		// de la clase dto estamos modificando los datos de acuerdo a los datos de entrada declarados arriba
		accountDTO.setNumCuenta(cuenta.longValue());
		accountDTO.setCdDivisa(divisa.toString());
		accountDTO.setCdTipoCuenta(tipoCuenta.toString());
		accountDTO.setImporte(importe.longValue());
		
		
		this.setListcuentas(uuaaR001.executeRead(cuenta,this.getPaginationin())); //metodo declarado en libreria UUAAR001
		this.setPaginationOut(uuaR001.executeGetPaginationOut(cuenta, paginationIn));
		
		Advice myAdvice = getAdvice();
		
		if (myAdvice != null){
			
			if(myAdvice.getCode().equals("UUAA0002")){
				this.setSeverity(Severity.ENR);
			}
		}
		
		
		
		this.setOperacion("YA NO ES INSERT");

		this.setOperacion("Fin de la op");
		LOGGER.info("TERMINO LA TRANSACCION 1");
		
	}
//UNA TRANSACCION SOLO PUEDE HACER UNA OPERACION REFERNTE A LA BASE DE DATOS INSERT,DELETE,UPDATE,ETC.
	//SI SE NECESITAN MAS OPERACIONES HACER OTRA TRANSACCION
}
